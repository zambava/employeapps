var AWS = require("aws-sdk");
var config = require('../config/index');
var DB = config.database;

AWS.config.update(DB.credential);

var dd = new AWS.DynamoDB();
var ddClient = new AWS.DynamoDB.DocumentClient();

function createTable(callback) {
    var params = {
        TableName: DB.name,
        KeySchema: [
            { AttributeName: "username", KeyType: "HASH" }
        ],
        AttributeDefinitions: [
            { AttributeName: "username", AttributeType: "S" }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits: 10
        }
    };

    dd.createTable(params, callback);
};

function put(items, callback, update) {
    var params = {
        TableName: DB.name,
        Item: items
    };

    if (!update)
        params.ConditionExpression = "attribute_not_exists(username)";

    ddClient.put(params, callback);
};

function findById(id, callback) {
    var params = {
        TableName: DB.name,
        KeyConditionExpression: "#un = :username",
        ExpressionAttributeNames: {
            "#un": "username"
        },
        ExpressionAttributeValues: {
            ":username": id
        }
    };

    ddClient.query(params, callback);
}

function deleteTable(callback) {
    dd.deleteTable({ TableName: DB.name }, callback);
}

function employe(callback) {
    var params = {
        TableName: DB.name };

    ddClient.scan(params, callback);
}

function update(items, callback) {
    var params = {
        TableName: DB.name,
        Key: {
            "username": {
                "S": items.username
            }
        },
        UpdateExpression: "SET #adm=:super, #pwd=:sandi",
        ExpressionAttributeNames: {
            "#adm": "admin",
            "#pwd": "password"
        },
        ExpressionAttributeValues: {
            ":super": {
                "BOOL": items.admin
            },
            ":sandi": {
                "S": items.password
            }
        }
    };

    dd.updateItem(params, callback);
}

function deleteItem(username, callback) {
    var params = {
        TableName: DB.name,
        Key: {
            "username": username
        }
    };

    ddClient.delete(params, callback);
}

function auth(username, callback) {
    var params = {
        TableName: DB.name,
        KeyConditionExpression: "#un = :username",
        ExpressionAttributeNames: {
            "#un": "username"
        },
        ExpressionAttributeValues: {
            ":username": username
        }
    };

    ddClient.query(params, callback);
}

function getCuti(callback){
    var params = {
        TableName: DB.name,
        ProjectionExpression: "username, cuti_log, info"
    };

    ddClient.scan(params, callback);
}

module.exports = {
    createTable: createTable,
    deleteTable: deleteTable,
    put: put,
    findById: findById,
    employe: employe,
    update: update,
    deleteItem: deleteItem,
    auth: auth,
    getCuti: getCuti
}
