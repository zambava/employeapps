var express = require("express");
var route = express.Router();
var fs = require("fs");
var db = require("../db/dynamoDB");
var jwt = require("jsonwebtoken");

var allEmploye = JSON.parse(fs.readFileSync('./employe.json', 'utf8'));

var validationParams = {
    'name': {
        notEmpty: true,
        errorMessage: 'Invalid name'
    },
    'password': {
        notEmpty: true,
        errorMessage: 'Invalid password'
    },
    'username': {
        notEmpty: true,
        errorMessage: 'Invalid username'
    }
};

route.post('/auth', function(req, res) {
    if (req.body.password == "" || req.body.username == "") {
        res.send("Authentication failed.", 400);
    } else {
        db.auth(req.body.username, function(err, data) {
            if (err || data.Count == 0) {
                res.send("Authentication failed.", 404);
            } else {
                var User = data.Items[0];
                if (User.password == req.body.password) {
                    if (User.admin == "true" || User.admin == true) {
                        var token = jwt.sign(User, 'secretCode', {
                            expiresIn: 1440
                        });

                        // set x-access-token header
                        // res.set('x-access-token', token);

                        res.json({
                            success: true,
                            message: 'Enjoy your token!',
                            token: token
                        });


                    } else {
                        res.send('Authentication failed. Don\'t have credential!', 403);
                    }
                } else {
                    res.send("Authentication failed.", 400);
                }
            }
        });
    }
});

route.get('/create', function(req, res) {
    db.createTable(function(err, data) {
        if (err) {
            res.send(err, 400);
        } else {
            allEmploye.forEach(function(employe) {
                var Item = {
                    "username": employe.username,
                    "password": employe.password,
                    "admin": employe.admin,
                    "info": employe.info,
                    "cuti_log": employe.cuti_log
                };

                db.put(Item, function(err, data) {
                    if (err) {
                        console.log("Cannot insert employe: " + employe.info.name);
                    } else {
                        console.log("Put employe success: " + employe.info.name);
                    }
                });
            });
            res.json({ success: true });
        }
    });
});

route.get('/delete', function(req, res) {
    db.deleteTable(function(err, data) {
        if (err) {
            res.send(err, 400);
        } else {
            res.json({ success: true });
        }
    });
});

route.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {

        jwt.verify(token, 'secretCode', function(err, decoded) {
            if (err) {
                return res.send('Invalid or token is already expired.', 498);
            } else {
                req.decoded = decoded;
                next();
            }
        });

    } else {
        return res.send({
            success: false,
            message: 'No token provided.'
        }, 403);
    }
});

route.get('/employe', function(req, res) {
    db.employe(function(err, users) {
        if (err) {
            res.send("Bad Request!", 400);
        } else {
            res.json(users);
        }
    })
});

route.get('/employe/cuti', function(req, res) {
    db.getCuti(function(err, data) {
        if (!err)
            res.json(sortCuti(data.Items));
        else
            res.sendStatus(404);
    });
});

route.post('/employe/create', function(req, res) {
    req.checkBody(validationParams);

    var errors = req.validationErrors();
    if (errors)
        res.send(errors, 400);
    else {
        var obj = {};
        for (var key in req.body) {
            if (req.body[key].length > 0) obj[key] = req.body[key]
        }
        var params = {
            username: obj.username,
            password: obj.password,
            admin: obj.admin,
            info: {
                name: obj.name,
                address: obj.address,
                gender: obj.gender,
                birthDate: obj.birthDate,
                joinDate: obj.joinDate,
                ktp: obj.ktp,
                position: obj.position,
                avatar: obj.avatar
            },
            cuti_log: []
        };

        db.put(params, function(err, data) {
            if (err) {
                res.send(err, 400);
            } else {
                res.json({ success: true, data: req.body });
            }
        });
    }
});

route.param('username', function(req, res, next, id) {
    db.findById(id, function(err, data) {
        if (err || data.Count == 0) {
            res.send('Employe not found.', 404);
        } else {
            var employe = data.Items[0];
            req.employe = employe;
            next();
        }
    });
});

route.route("/employe/:username").get(function(req, res) {
    res.json(req.employe);
}).put(function(req, res, next) {
    req.checkBody(validationParams);

    var errors = req.validationErrors();

    if (errors)
        res.send(errors, 400);
    else {
        var params = {
            username: req.employe.username,
            password: req.body.password,
            admin: req.body.admin,
            info: {
                name: req.body.name,
                address: req.body.address,
                gender: req.body.gender,
                birthDate: req.body.birthDate,
                joinDate: req.body.joinDate,
                ktp: req.body.ktp,
                position: req.body.position,
                avatar: req.body.avatar
            },
            cuti_log: req.body.cuti_log
        }

        db.put(params, function(err, data) {
            if (err) {
                res.send(err, 400);
            } else {
                res.json({ success: true });
            }
        }, true);
    }
}).delete(function(req, res) {
    if (req.body.admin == "true" || req.body.admin == true) {
        res.send("User admin tidak bisa dihapus!", 403);
    } else {
        db.deleteItem(req.employe.username, function(err, data) {
            if (err) {
                res.json({ success: false });
            } else {
                res.json({ success: true });
            }
        });
    }
});

// function
function sortCuti(obj) {
    var arr = [];

    obj.forEach(function(a, b) {
        a.cuti_log.forEach(function(cuti, id) {
            cuti.user = a.username;
            cuti.index = id;
            cuti.name = a.info.name;

            arr.push(cuti);
        });
    });

    return arr
}

module.exports = route;