var React = require("react");
var ReactDOM = require("react-dom");
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var _ = require("lodash");
var Modal = require("react-bootstrap").Modal;

Date.prototype.yyyymmdd = function() {
	var yyyy = this.getFullYear().toString();
	var mm = (this.getMonth()+1).toString();
	var dd  = this.getDate().toString();
	return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]);
};

Date.prototype.addDays = function(d){
	return new Date(this.valueOf()+864E5*d);
}

var ModalCuti = React.createClass({
	mixins: [LinkedStateMixin],

	getInitialState: function(){
		return {}
	},

	flatten: function(obj, result, prefix) {
	    if(_.isObject(obj)) {
	        _.each(obj, function(val, key) {
	        	if(key != "cuti_log")
		            this.flatten(val, result, key);
	        }.bind(this))
	    }
	    else
	    	result[prefix] = obj; 

	    return result
	},

	tambah: function(){
		var obj = {},
			cuti_log = this.state.cuti_log;

		obj.mulai = this.state.dari;
		obj.sampai = new Date(this.state.dari).addDays(this.state.sampai).yyyymmdd();

		if(this.state.keterangan.length > 0)
			obj.keterangan = this.state.keterangan;

		cuti_log.push(obj);

		this.setState({
			cuti_log: cuti_log
		});

		var data = this.state;

		console.log(data);

	    $.ajax({
	      url: "http://localhost:3000/api/employe/" + this.state.username + "?token=" + localStorage.token,
	      type: "PUT",
	      data: data,
	      success: function(){
	      	alert("Cuti karyawan berhasil ditambahkan");
	      	this.props.hide();
	      }.bind(this),
	      error: function(err) {
	        if(err.status == 400)
	        	alert("Bad request");
	      }
	    });
	},

	componentWillReceiveProps: function(nextProps){
		this.state = new Object();

		this.setState({
			dari: new Date().yyyymmdd(),
			sampai: 1,
			keterangan: ''
		});

		this.setState(this.flatten(nextProps.data, {}));
		this.setState({cuti_log: nextProps.data.cuti_log});
	},

	render: function(){
		return (
			<Modal show={this.props.show} onHide={this.props.hide}>
	            <Modal.Header closeButton>
	              <Modal.Title>Tambah Cuti Karyawan</Modal.Title>
	            </Modal.Header>

	            <Modal.Body>
	              <div className="row">
	              	<div className="form-horizontal">
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Dari Tanggal</label>
	              			<div className="col-sm-8">
	              				<input type="date" className="form-control" valueLink={this.linkState('dari')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Lama Cuti</label>
	              			<div className="col-sm-8">
	              				<div className="input-group">
							        <input type="number" min="1" max="12" className="form-control" valueLink={this.linkState('sampai')}/>
							        <span className="input-group-addon">hari</span>
							    </div>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Keterangan</label>
	              			<div className="col-sm-8">
	              				<textarea className="form-control" valueLink={this.linkState('keterangan')}></textarea>
	              			</div>
	              		</div>
	              	</div>
	              </div>
	            </Modal.Body>
	            <Modal.Footer>
	            	<button className="btn btn-danger" onClick={this.props.hide}>Batal</button>
	            	<button className="btn btn-primary" onClick={this.tambah}>Tambah</button>
	            </Modal.Footer>
	        </Modal>
        )
	}
});

module.exports = ModalCuti;