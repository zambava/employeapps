var React = require("react");
var ReactDOM = require("react-dom");
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var _ = require("lodash");
var Notification = require('./notif.jsx');

var FormEdit = React.createClass({
	mixins: [LinkedStateMixin],

	getInitialState: function(){
		return {
			admin: false
		}
	},

	flatten: function(obj, result, prefix) {
	    if(_.isObject(obj)) {
	        _.each(obj, function(val, key) {
	        	if(key != "cuti_log")
		            this.flatten(val, result, key);
	        }.bind(this))
	    }
	    else
	    	result[prefix] = obj; 

	    return result
	},

	handleUpdate: function(){
		var data = this.state;

	    $.ajax({
	      url: "http://localhost:3000/api/employe/" + this.state.username + "?token=" + localStorage.token,
	      type: "PUT",
	      data: data,
	      success: function(){
	      	window.location.href = "users.html";
	      },
	      error: function(err) {
	        if(err.status == 400)
	        	alert("Bad request, cek kembali field karyawan");
	      }
	    });
	},

	componentWillReceiveProps: function(nextProps){
		this.setState(this.flatten(nextProps.employe, {}));
		this.setState({cuti_log: nextProps.employe.cuti_log});
	},

	render: function(){
		return(
			<div className="form clearfix">
				<div className="col-sm-3 avatar">
					<img src={this.props.employe.info.avatar || "img/default-avatar.png"}/>
				</div>
				<div className="col-sm-3">
					<div className="form-group">
						<strong>Nama</strong>
						<input type="text" className="form-control" valueLink={this.linkState('name')}/>
					</div>
					<div className="form-group">
						<strong>Nama Pengguna</strong>
						<input type="text" disabled className="form-control" valueLink={this.linkState('username')}/>
					</div>
					<div className="form-group">
						<strong>Kata Sandi</strong>
						<input type="password" className="form-control" valueLink={this.linkState('password')}/>
					</div>
					<div className="form-group">
						<strong>Tanggal Lahir</strong>
						<input type="date" className="form-control" valueLink={this.linkState('birthDate')}/>
					</div>
					<div className="form-group">
						<strong>KTP</strong>
						<input type="text" className="form-control" valueLink={this.linkState('ktp')}/>
					</div>
				</div>
				<div className="col-sm-3">
					<div className="form-group">
						<strong>Posisi</strong>
						<input type="text" className="form-control" valueLink={this.linkState('position')}/>
					</div>
					<div className="form-group">
						<strong>Jenis Kelamin</strong>
						<select valueLink={this.linkState('gender')} name="gender" className="form-control">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
	                    </select>
					</div>
					<div className="form-group">
						<strong>Tanggal Bergabung</strong>
						<input type="date" className="form-control" valueLink={this.linkState('joinDate')}/>
					</div>
					<div className="form-group">
						<strong>Alamat</strong>
						<textarea className="form-control" valueLink={this.linkState('address')}></textarea>
					</div>
					<div className="checkbox" style={{display: "none"}}>
						<label>
							<input type="checkbox" checkedLink={this.linkState('admin')}/> Admin
						</label>
					</div>
				</div>
				<div className="clearfix"></div>
				<div className="col-sm-offset-6 col-sm-3 text-right">
					<a onClick={this.handleUpdate} className="btn btn-primary">Update</a>
				</div>
			</div>
		)
	}
});

var EditPanel = React.createClass({
	getInitialState: function(){
		return {
			employe: {
				info: {
		          name: '',
		          ktp: '',
		          address: '',
		          gender: '',
		          joinDate: '',
		          avatar: '',
		          position: '',
		          birthDate: ''
		        },
		        cuti_log: []
			}
		}
	},

	getQueryVariable: function(variable) {
	    var variable = variable.toLowerCase(),
	        query = window.location.search.substring(1).toLowerCase(),
	        vars = query.split("&");

	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split("=");
	        if (pair[0] == variable) {
	            return pair[1];
	        }
	    }
	    return (false);
	},

	getData: function(){
		var token = localStorage.token,
			url = this.props.url + this.getQueryVariable('employe') + "?token=" + token;

		$.ajax({
			url: url,
			dataType: 'json',
			success: function(data){
				this.setState({ employe : data });
			}.bind(this)
		})
	},

	componentDidMount: function(){
		this.getData();
	},

	render: function(){
		return (
			<div>
				<FormEdit employe={this.state.employe}/>
			</div>
		)
	}
});

ReactDOM.render(
	<EditPanel url="http://localhost:3000/api/employe/"/>,
	document.getElementById("employeEdit")
);

// Notification
ReactDOM.render(
  <Notification url="http://localhost:3000/api/employe/cuti"/>,
  document.getElementById("notification")
);