var React = require("react");
var ReactDOM = require("react-dom");
var _ = require("lodash");
var Notification = require('./notif.jsx');

var FormStatic = React.createClass({
	getInitialState: function(){
		return{
			lama: '',
			totalCuti: 0
		}
	},

	calcDate: function(tgl){
		var DateNow = new Date();
		var JoinDate = new Date(tgl);

	    var diff = Math.floor(DateNow.getTime() - JoinDate.getTime());

	    var day = 1000 * 60 * 60 * 24;

	    var days = Math.floor(diff/day);
	    var months = Math.floor(days/31);
	    var years = Math.floor(months/12);

	    var message = "";

	    if(months > 0 && months < 12)
	    	message = months + " bulan";
	    else if(months >= 12)
	    	message = years + " tahun";
	    else
	    	message = days + " hari"

	    this.setState({
	    	lama: message
	    });
    },

    daydiff: function(first, second) {
    	var dari = new Date(first);
    	var sampai = new Date(second);

	    return Math.round((sampai-dari)/(1000*60*60*24));
	},

    hitungCuti: function(obj) {
		var now = Date.now();
			totalCuti = this.state.totalCuti;

    	obj.map(function(cuti){
			var tglCuti = new Date(cuti.mulai).getTime();
			if(now > tglCuti)
			{
				totalCuti+=this.daydiff(cuti.mulai, cuti.sampai);
			}
    	}.bind(this));
    	this.setState({
			totalCuti: totalCuti
		});
    },

    componentWillReceiveProps: function(nextProps){
		this.hitungCuti(nextProps.employe.cuti_log);
    	this.calcDate(nextProps.employe.info.joinDate);
    },

	render: function(){
		return(
			<div className="form clearfix">
				<div className="col-sm-3 avatar">
					<img src={this.props.employe.info.avatar || "img/default-avatar.png"}/>
				</div>
				<div className="col-sm-3">
					<div className="form-group">
						<strong>Nama</strong>
						<p className="form-control-static">{this.props.employe.info.name}</p>
					</div>
					<div className="form-group">
						<strong>Nama Pengguna</strong>
						<p className="form-control-static">{this.props.employe.username}</p>
					</div>
					<div className="form-group">
						<strong>Tanggal Lahir</strong>
						<p className="form-control-static">{new Date(this.props.employe.info.birthDate).toLocaleDateString()}</p>
					</div>
					<div className="form-group">
						<strong>KTP</strong>
						<p className="form-control-static">{this.props.employe.info.ktp}</p>
					</div>
					<div className="form-group">
						<strong>Alamat</strong>
						<p className="form-control-static">{this.props.employe.info.address}</p>
					</div>
				</div>
				<div className="col-sm-3">
					<div className="form-group">
						<strong>Posisi</strong>
						<p className="form-control-static">{this.props.employe.info.position}</p>
					</div>
					<div className="form-group">
						<strong>Jenis Kelamin</strong>
						<p className="form-control-static">{this.props.employe.info.gender}</p>
					</div>
					<div className="form-group">
						<strong>Total Cuti</strong>
						<p className="form-control-static">{this.state.totalCuti}</p>
					</div>
					<div className="form-group">
						<strong>Tanggal Bergabung</strong>
						<p className="form-control-static">{new Date(this.props.employe.info.joinDate).toLocaleDateString()}</p>
					</div>
					<div className="form-group">
						<strong>Lama Bergabung</strong>
						<p className="form-control-static">{this.state.lama}</p>
					</div>
				</div>
			</div>
		)
	}
});

var CutiRow = React.createClass({
	getInitialState: function(){
		return {
			status: ''
		}
	},

	componentDidMount: function(){
		var now = Date.now();
		var tglCuti = new Date(this.props.cuti.mulai).getTime();
		var tglSampai = new Date(this.props.cuti.sampai).getTime();
		var status = "";

		if(now < tglCuti)
			status = "Belum";
		else if(now > tglCuti && now < tglSampai)
			status = "Berlangsung";
		else
			status = "Sudah";

		this.setState({
			status: status
		});
		
	},

	render: function(){
		return (
			<tr>
				<td>{new Date(this.props.cuti.mulai).toLocaleDateString()}</td>
				<td>{new Date(this.props.cuti.sampai).toLocaleDateString()}</td>
				<td>{this.props.cuti.keterangan}</td>
				<td>{this.state.status}</td>
			</tr>
		)
	}
});

var TableCuti = React.createClass({
	getInitialState: function(){
		return {
			cuti: []
		}
	},

	_sort: function(obj, by, ascDesc){
		var by = by || "mulai",
			ascDesc = ascDesc || "asc",
			sorted = _.orderBy(obj, by, ascDesc);
		
		this.setState({
			cuti: sorted
		});
	},

	componentWillReceiveProps: function(nextProps){
		this._sort(nextProps.history, "mulai", "desc");
	},

	render : function(){
		var row = [];
		var list = this.state.cuti.map(function(list){
				row.push(<CutiRow cuti={list} key={Math.random()}/>);
		});
		return (
			<table className="table table-bordered text-center">
				<thead>
					<tr>
						<th className="text-center">Tanggal Mulai</th>
						<th className="text-center">Sampai Tanggal</th>
						<th className="text-center">Keterangan</th>
						<th className="text-center">Status</th>
					</tr>
				</thead>
				<tbody>
					{row}
				</tbody>
			</table>
		)
	}
});

var CutiPanel = React.createClass({
	render: function(){
		return (
			<div className="clearfix">
				<div className="col-sm-offset-2 col-sm-8">
					<div className="text-center">
						<h4>Tabel Cuti Karyawan</h4>
					</div>
					<div className="table-responsive">
						<TableCuti history={this.props.employe.cuti_log}/>
					</div>
				</div>
			</div>
		)
	}
});

var DetailPanel = React.createClass({
	getInitialState: function(){
		return {
			employe: {
				info: {
		          name: '',
		          ktp: '',
		          address: '',
		          gender: '',
		          joinDate: '',
		          avatar: '',
		          position: '',
		          birthDate: ''
		        },
		        cuti_log: []
			}
		}
	},

	getQueryVariable: function(variable) {
	    var variable = variable.toLowerCase(),
	        query = window.location.search.substring(1).toLowerCase(),
	        vars = query.split("&");

	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split("=");
	        if (pair[0] == variable) {
	            return pair[1];
	        }
	    }
	    return (false);
	},

	getData: function(){
		var token = localStorage.token,
			url = this.props.url + this.getQueryVariable('employe') + "?token=" + token;

		$.ajax({
			url: url,
			dataType: 'json',
			success: function(data){
				this.setState({ employe : data });
			}.bind(this)
		})
	},

	componentDidMount: function(){
		this.getData();
	},

	render: function(){
		return (
			<div>
				<FormStatic employe={this.state.employe}/>
				<CutiPanel employe={this.state.employe}/>
			</div>
		)
	}
});

ReactDOM.render(
	<DetailPanel url="http://localhost:3000/api/employe/"/>,
	document.getElementById("employeDetail")
);

// Notification
ReactDOM.render(
  <Notification url="http://localhost:3000/api/employe/cuti"/>,
  document.getElementById("notification")
);