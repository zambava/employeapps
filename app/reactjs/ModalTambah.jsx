var React = require("react");
var ReactDOM = require("react-dom");
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var Modal = require("react-bootstrap").Modal;

var ModalTambah = React.createClass({
	mixins: [LinkedStateMixin],

	getInitialState: function(){
		return {
			birthDate: "2016-03-20",
			joinDate: "2016-03-20",
			admin: false,
			gender: "Male"
		}
	},

	tambah: function(){
		var data = this.state;
		data.token = localStorage.token;

		if(data.password != data.confirm_password){
			alert("konfirmasi password salah");
			return false;
		}

	    $.ajax({
	      url: "http://localhost:3000/api/employe/create",
	      type: "POST",
	      data: data,
	      dataType: 'json',
	      success: function(){
	      	alert("Data karyawan berhasil ditambahkan");
	      	this.props.hide();
	      }.bind(this),
	      error: function(a, b, c) {
	        alert('Gagal menambah data karyawan, cek kembali form karyawan');
	      }
	    });
	},

	render: function(){
		return (
			<Modal show={this.props.show} onHide={this.props.hide}>
	            <Modal.Header closeButton>
	              <Modal.Title>Tambah Data Karyawan</Modal.Title>
	            </Modal.Header>

	            <Modal.Body>
	              <div className="clearfix">
	              	<div className="form-horizontal">
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Nama</label>
	              			<div className="col-sm-8">
	              				<input type="text" className="form-control" valueLink={this.linkState('name')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Posisi</label>
	              			<div className="col-sm-8">
						        <input type="text" className="form-control" valueLink={this.linkState('position')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Nama Pengguna</label>
	              			<div className="col-sm-8">
						        <input type="text" className="form-control" valueLink={this.linkState('username')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Password</label>
	              			<div className="col-sm-8">
						        <input type="password" className="form-control" valueLink={this.linkState('password')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Konfirmasi Password</label>
	              			<div className="col-sm-8">
						        <input type="password" className="form-control" valueLink={this.linkState('confirm_password')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Tanggal Lahir</label>
	              			<div className="col-sm-8">
						        <input type="date" className="form-control" valueLink={this.linkState('birthDate')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">KTP</label>
	              			<div className="col-sm-8">
						        <input type="text" className="form-control" valueLink={this.linkState('ktp')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Jenis Kelamin</label>
	              			<div className="col-sm-8">
						        <select valueLink={this.linkState('gender')} name="gender" className="form-control">
									<option value="Male">Male</option>
									<option value="Female">Female</option>
			                    </select>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Tanggal Bergabung</label>
	              			<div className="col-sm-8">
						        <input type="date" className="form-control" valueLink={this.linkState('joinDate')}/>
	              			</div>
	              		</div>
	              		<div className="form-group">
	              			<label className="col-sm-3 control-label">Alamat</label>
	              			<div className="col-sm-8">
	              				<textarea className="form-control" valueLink={this.linkState('address')}></textarea>
	              			</div>
	              		</div>
	              		<div className="checkbox col-sm-offset-3">
							<label>
								<input type="checkbox" checkedLink={this.linkState('admin')}/> Admin
							</label>
						</div>
	              	</div>
	              </div>
	            </Modal.Body>
	            <Modal.Footer>
	            	<button className="btn btn-danger" onClick={this.props.hide}>Batal</button>
	            	<button className="btn btn-primary" onClick={this.tambah}>Tambah</button>
	            </Modal.Footer>
	        </Modal>
		)
	}
});

module.exports = ModalTambah;