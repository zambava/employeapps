var React = require("react");
var ReactDOM = require("react-dom");
var ModalCuti = require('./ModalCuti.jsx');
var ModalTambah = require('./ModalTambah.jsx');

var Notification = require('./notif.jsx');

$.fn.serializeObject = function()
{
    var obj = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if(this.value.length > 0)
          obj[this.name] = this.value
    });
    return obj;
};

var SearchBar = React.createClass({
  handleChange: function() {
    this.props.onUserInput(
      this.refs.filterTextInput.value
    );
  },
  render: function() {
    return (
      <div className="form">
        <input className="form-control" type="text" placeholder="Search..." value={this.props.filterText} ref="filterTextInput" onChange={this.handleChange}/>
      </div>
    );
  }
});

var EmployesList = React.createClass({
  clickHandler: function(){
    this.props.callback(this.props.data);
  },

  deleteEmploye: function(e){
    e.preventDefault();
    if(confirm("Yakin ingin menghapus data karyawan?"))
    {
      var data = this.props.data;
      var username = this.props.data.username;
      $.ajax({
        url: "http://localhost:3000/api/employe/" + username + "?token=" + localStorage.token,
        type: "DELETE",
        data: data,
        success: function(data){
          this.props.callbackDelete();
        }.bind(this),
        error: function(err){
          alert(err.responseText);
        }
      });
    }
  },

  render: function(){
    return (
      <li>
        <a className="employe" onClick={this.clickHandler}>
          <strong>{this.props.data.info.name}</strong>
          <p>{this.props.data.info.position}</p>
        </a>
        <a onClick={this.deleteEmploye} className="delete btn btn-danger btn-sm">
          <i className="fa fa-trash"></i>
        </a>
      </li>
    )
  }
});

var AddButton = React.createClass({
  getInitialState: function(){
    return {
      modalTambahShow: false
    }
  },

  modalHide: function(){
    this.setState({
      modalTambahShow: false
    })
  },

  modalShow: function(){
    this.setState({
      modalTambahShow: true
    })
  },

  render: function(){
    return (
      <li className="text-center" style={{padding: "10px 0"}}>
        <a className="btn btn-primary" onClick={this.modalShow}>Tambah karyawan</a>
        <ModalTambah show={this.state.modalTambahShow} hide={this.modalHide}/>
      </li>
    ) 
  }
});

var DetailPanel = React.createClass({
  getInitialState: function(){
    return {
      modalShow: false
    }
  },

  modalCutiShow: function(){
    this.setState({
      modalShow: true
    });
  },

  modalCutiHide: function(){
    this.setState({
      modalShow: false
    });
  },

  render: function(){
    return (
      <div className="detail-panel">
        <div className="avatar text-center">
          <img src={this.props.data.info.avatar || "img/default-avatar.png"}/>
        </div>
        <div className="form-group">
          <strong>Nama</strong>
          <p className="form-control-static">{this.props.data.info.name}</p>
        </div>

        <div className="form-group">
          <strong>Posisi</strong>
          <p className="form-control-static">{this.props.data.info.position}</p>
        </div>

        <div className="form-group">
          <strong>KTP</strong>
          <p className="form-control-static">{this.props.data.info.ktp}</p>
        </div>

        <div className="form-group">
          <strong>Alamat</strong>
          <p className="form-control-static">{this.props.data.info.address}</p>
        </div>

        <div className="text-right">
          <ul className="list-inline">
            <li>
              <a onClick={this.modalCutiShow} className="btn btn-default"><i className="fa fa-plus"></i> Cuti</a>
            </li>
            <li>
              <a href={"detail.html?employe=" + this.props.data.username} className="btn btn-primary">Info</a>
            </li>
            <li>
              <a href={"edit.html?employe=" + this.props.data.username} className="btn btn-success">Edit</a>
            </li>
          </ul>
        </div>
        <ModalCuti hide={this.modalCutiHide} show={this.state.modalShow} data={this.props.data}/>
      </div>
    )
  }
});

var EmployesPanel = React.createClass({
  getInitialState: function(){
    return {
      employes: [],
      dataSelect: {
        info: {
          name: '',
          ktp: '',
          address: '',
          gender: '',
          joinDate: '',
          avatar: '',
          position: '',
          birthDate: ''
        },
        cuti_log: []
      },
      filterText: ''
    }
  },

  handleUserInput: function(val){
    this.setState({
      filterText: val
    })
  },

  getData: function(){
    var token = localStorage.token,
        url = this.props.url + "?token=" + token;

    $.ajax({
      url: url,
      dataType: 'json',
      success: function(data){
        this.setState({
          employes: data.Items,
          dataSelect: data.Items[0]
        });
      }.bind(this)
    });
  },

  employeSelect: function(data){
    this.setState({
      dataSelect: data
    });
  },

  componentDidMount: function(){
    this.getData();
    // setInterval(this.getData, 2000);
  },

  render: function(){
    var row = [];
    var list = this.state.employes.map(function(employe){
      var name = employe.info.name.toLowerCase(),
          query = this.state.filterText.toLowerCase();

      if(name.indexOf(query) < 0)
        return

      row.push(<EmployesList data={employe} callbackDelete={this.getData} callback={this.employeSelect} key={employe.username}/>);
    }.bind(this));

    return (
      <div className="user-list-panel clearfix">
        <div className="col-sm-8">
          <SearchBar filterText={this.state.filterText} onUserInput={this.handleUserInput}/>
          <ul className="list-unstyled employe-list-panel">
            {row}
            <AddButton/>
          </ul>
        </div>

        <div className="col-sm-4">
          <DetailPanel data={this.state.dataSelect}/>
        </div>
      </div>
    )
  }
});

ReactDOM.render(
  <EmployesPanel url="http://localhost:3000/api/employe"/>,
  document.getElementById("employePanel")
);

// Notification
ReactDOM.render(
  <Notification url="http://localhost:3000/api/employe/cuti"/>,
  document.getElementById("notification")
);