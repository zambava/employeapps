var React = require("react");
var ReactDOM = require("react-dom");

var FormLogin = React.createClass({
	getInitialState: function(){
		return {username: '', password: ''}
	},
	onChangeUsername: function(e){
		this.setState({ username : e.target.value })
	},
	onChangePassword: function(e) {
		this.setState({ password : e.target.value })	
	},
	handleSubmit: function(e){
		e.preventDefault();
		$.ajax({
			url: 'http://localhost:3000/api/auth',
			dataType: 'json',
			type: 'POST',
			data: this.state,
			success: function(data) {
				localStorage.setItem("token", data.token);
				window.location.href = "users.html";
			}.bind(this),
			error: function(err) {
				alert(err.responseText);
			}.bind(this)
		});
	},
	render: function(){
		return (
			<div className="wrapper">
			  	<form className="form-signin" onSubmit={this.handleSubmit}>
			  		<h2 className="form-signin-heading">Please login</h2>
			  		<input type="text" className="form-control" name="username" onChange={this.onChangeUsername} placeholder="Username" required/>
			  		<input type="password" className="form-control" name="password" onChange={this.onChangePassword} placeholder="Password" required/>
			  		<button className="btn btn-lg btn-primary btn-block" type="submit">Login</button>
			  	</form>
			</div>
		)
	}
});

ReactDOM.render(<FormLogin/>, document.getElementById('content'));