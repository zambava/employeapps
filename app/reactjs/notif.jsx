var React = require("react");
var ReactDOM = require("react-dom");
var _ = require("lodash");

var NotificationList = React.createClass({

	componentDidMount: function(){
		$(ReactDOM.findDOMNode(this.refs.dropList)).on("click", function(e){
			e.stopPropagation();
			console.log(this.props);
		}.bind(this));
	},

	render: function(){
		return(
			<li>
				<a href={"detail.html?employe=" + this.props.data.user} ref="dropList">
					<p className="username">{this.props.data.name}</p>
					<p className="desc">{this.props.data.keterangan}</p>
					<small>{new Date(this.props.data.mulai).toDateString()}</small>
				</a>
			</li>
		)
	}
});

var Notification = React.createClass({
	getInitialState: function(){
		return {
			notif: []
		}
	},

	sortNotif: function(by, ascDesc) {
		var by = by || "mulai",
			ascDesc = ascDesc || "asc",
			sorted = _.orderBy(this.state.notif, by, ascDesc);
		
		this.setState({
			notif: sorted
		});
	},

	getNotif: function(){
		var token = localStorage.token,
			url = this.props.url + "?token=" + token;

		$.ajax({
			url: url,
			dataType: "json",
			success: function(data){
				this.setState({
					notif: data
				});

				this.sortNotif();
			}.bind(this),
			error: function(err){
				if(err.status == 498)
					window.location.href = "/";
			}.bind(this)
		});
	},

	componentDidMount: function(){
		this.getNotif();
		setInterval(this.getNotif, 2000);
	},

	render: function(){
		var count = 0, list = this.state.notif.map(function(item){
			if(Date.now() < new Date(item.mulai).getTime())
			{
				count++;
				return (
					<NotificationList data={item} key={Math.random()}/>
				)
			}
		});

		return(
			<div className="dropdown">
				<div className="dropdown">
					<a className="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i className="fa fa-bell-o"></i> {count}
						<span className="caret"></span>
                    </a>

                    <ul className="dropdown-menu notif" aria-labelledby="dropdownMenu1">
                    	{list}
                    </ul>
                </div>
            </div>
		)
	}
});

module.exports = Notification;