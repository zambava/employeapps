#STEP 1
Buka folder DynamoDB kemudian jalankan DynamoDB local dengan menggunakan PORT ```8080```:
```
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -port 8080
```

#STEP 2
Buka folder ```api/``` kemudian install module dengan command berikut:
```
npm install
```
Setelah menginstall module menggunakan npm install, jalankan ```api``` dengan menggunakan command seperti berikut:
```
node bin/www
```

#STEP 3
Buka folder ```app/``` kemudian install module dengan command berikut:
```
npm install
```
Jalankan file ```server.js``` (harusnya listening ke port ```3333```)
```
node server.js
```

#STRUCTURE DATABASE:
```
{
  username: "HASH",
  password: 'string',
  admin: 'boolean',
  cuti_log: 'Array',
  info: {
    name: 'string',
    dateBirth: 'string',
    joinDate: 'string',
    address: 'string',
    ktp: 'number',
    gender: 'string',
    avatar: 'string',
    position: 'string'
  }
}
```

#CREATE TABLE:
Untuk membuat table dan menambah data karyawan (dummy) copy/paste url ```http://localhost:3000/api/create``` di browser dan akan muncul ```{success: true}``` jika table sudah berhasil dibuat.

#DELETE TABLE:
Untuk menghapus table copy/paste url ```http://localhost:3000/api/delete``` di browser dan akan muncul ```{success: true}``` jika table sudah berhasil dihapus.

#EMPLOYE APP:
Copy/paste url ```http://localhost:3333/``` pada browser
Login menggunakan credential
```
username: admin
password: 12345
```